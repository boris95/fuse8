var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    browserSync = require('browser-sync').create(),
    sourceMaps = require('gulp-sourcemaps'),
    gcmq = require('gulp-group-css-media-queries'),
    preproc = require('gulp-less'),
    rename = require('gulp-rename'),
    rsync = require('gulp-rsync'),
    gulpver = '4';

const config = {
    src: './src',
    css: {
        watch: '/precss/**/*.less',
        src: '/precss/+(styles|styles-ie).less',
        dest: '/css'
    },
    html: {
        src: '/*.html'
    }
};

gulp.task('deploy', function() {
    return gulp.src(config.src)
        .pipe(rsync({
            root: 'src/',
            hostname: '',
            destination: '',
            exclude: ['**/Thumbs.db','**/*.DS_Store'],
            archive: true,
            silent: false,
            compress: true,
            port: 9999,
            recursive: true
        }));
});

gulp.task('browserSync', function () {
     browserSync.init({
        server: {
            baseDir: config.src
        },
        notify: false,
        // open: false,
        // online: false,
        // tunnel: true, tunnel: "projectname",
    });
});

gulp.task('minCss', function () {
    return gulp.src(config.src + config.css.src)
        .pipe(sourceMaps.init())
        .pipe(preproc())
        .pipe(gcmq())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({
            level: 2
        }))
        .pipe(sourceMaps.write())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest(config.src + config.css.dest));
});

gulp.task('css', function () {
    return gulp.src(config.src + config.css.src)
        .pipe(preproc())
        .pipe(gcmq())
        .pipe(autoprefixer({
            browsers: ['> 0.1%'],
            cascade: false
        }))
        .pipe(gulp.dest(config.src + config.css.dest))
        .pipe(browserSync.stream());
});

gulp.task('build', function(){
   gulp.parallel('css', 'minCss');
});

gulp.task('build', gulp.parallel(
    'css', 'minCss'
));

gulp.task('copyTo', function () {
    return gulp.src(config.src + config.css.dest+'/*.*').pipe(gulp.dest(config.css.project));
});

gulp.task('code', function(){
    return gulp.src(config.src + config.html.src)
        .pipe(browserSync.reload({ stream: true }));
});


if ( gulpver === '3' ){
    gulp.task('watch', ['css','browserSync'], function () {
        gulp.watch(config.src + config.css.watch, ['css']);
        gulp.watch(config.src + config.html.src, browserSync.reload);
    });

    gulp.task('default', ['watch']);
}

if ( gulpver === '4' ){

    gulp.task('watch', function () {
        gulp.watch(config.src + config.css.watch, gulp.parallel('css'));
        gulp.watch(config.src + config.html.src, gulp.parallel('code'));
    });

    gulp.task('default', gulp.parallel('watch', 'css', 'browserSync'));

}