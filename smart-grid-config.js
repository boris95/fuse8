var smartgrid = require('smart-grid');

/* It's principal settings in smart grid project */
var settings = {
    outputStyle: 'less',
    columns: 12,
    offset: '30px',
    mobileFirst: false,
    container: {
        maxWidth: '1180px',
        fields: '30px'
    },
    oldSizeStyle: false
    ,
    breakPoints: {
        lg: {
            width: '1100px',
            fields: '30px'
        },
        md: {
            width: '960px',
            fields: '30px'
        },
        sm: {
            width: '769px',
            fields: '15px' /* set fields only if you want to change container.fields */
        },
        xs: {
            width: '560px',
            fields: '15px'
        }
    }
        /*
        We can create any quantity of break points.

        some_name: {
            width: 'Npx',
            fields: 'N(px|%|rem)',
            offset: 'N(px|%|rem)'
        }
        */
    // }
};

smartgrid('./src/precss', settings);